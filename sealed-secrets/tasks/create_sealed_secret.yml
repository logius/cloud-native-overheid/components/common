#
# Create a new SealedSecret, apply it to the namespace and save a backup in the S3 bucket. 
#
# Parameters:
#   - s3_bucket: name of the s3 bucket
#   - s3_endpoint: fqdn of s3 (without protocol)
#   - labels: optional labels for the secret
#   - secret_data: key value map with secrets
#   - secret_name: name of the secret. 
#   - secret_namespace: namespace where the secret will be created
#   - overwrite_existing_secret: overwrite existing secret if it was not created by the sealed-secrets controller
#
# The filename of the sealed secret will have "-sealed-secret.yaml" appended to the secret name. 
# 
# Example: 
#   - name: Create a sealed secret
#     include_role:
#       name: common/sealed-secrets
#       tasks_from: "create_sealed_secret.yml"
#     vars: 
#       labels:
#         my_label: "something"
#       secret_data:
#         username: "my_username"
#         password: "verysecret"
#       secret_name: my-secret"
#       secret_namespace: my-namespace
#       overwrite_existing_secret: true

- name: Annotate existing secret to be managed by the sealed-secrets controller
  block:
    - name: Get existing secret
      k8s_info:
        name: "{{ secret_name }}"
        kind: Secret
        namespace: "{{ secret_namespace }}"
      register: existing_secret_to_seal
    - name: Annotate existing secret
      shell: |
        kubectl annotate secret {{ secret_name }} -n {{ secret_namespace }} "sealedsecrets.bitnami.com/managed"="true" --overwrite
      when: existing_secret_to_seal.resources | length > 0
  when: overwrite_existing_secret|default(false) 

- name: Get S3 secret to write secrets in bucket
  k8s_info:
    name: s3-secret
    kind: Secret
    namespace: "{{ secret_namespace }}"
  register: s3_secret

- name: local sealed secret file 
  set_fact: 
    local_secret_yaml_file: "{{ install_dir }}/{{ secret_name }}-secret.yaml"
    local_sealed_secret_yaml_file: "{{ install_dir }}/{{ secret_name }}-sealed-secret.yaml"

- name: Create secret definition
  set_fact:
    secret_manifest:
      apiVersion: v1
      kind: Secret
      type: Opaque
      metadata:
        name: "{{ secret_name }}"
        namespace: "{{ secret_namespace }}"
        labels: "{{ labels | default (omit)}}"
      data: "{{ secret_data }}"

- name: Write secret to local file
  copy:
    dest: "{{ local_secret_yaml_file }}"
    content: "{{ secret_manifest | to_nice_yaml }}"

- name: Create sealed secret 
  shell: >
    kubeseal -f {{ local_secret_yaml_file }} --controller-name=sealed-secrets --controller-namespace={{ config.sealed_secrets.namespace }} 
    --scope cluster-wide
    --format yaml > {{ local_sealed_secret_yaml_file }}

- name: Apply sealed-secret to namespace 
  k8s:
    state: present
    apply: true
    definition: "{{ lookup('file', local_sealed_secret_yaml_file) }}"

- name: Upload sealed secret to S3 
  amazon.aws.aws_s3:
    s3_url: "https://{{ s3_endpoint }}"
    aws_access_key: "{{ s3_secret.resources[0].data['accesskey'] | b64decode }}"
    aws_secret_key: "{{ s3_secret.resources[0].data['secretkey'] | b64decode }}"
    bucket: "{{ s3_bucket }}"
    object: "secrets/{{ secret_name }}-sealed-secret.yaml"
    src: "{{ local_sealed_secret_yaml_file }}"
    encrypt: false
    mode: put
  when: s3_secret.resources is defined and (s3_secret.resources |length>0)
    