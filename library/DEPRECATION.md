
The modules in this project are deprecated in Ansible v5 and are removed. 

If you really need these modules please use a tag of common, e.g. v1.1.9.

Upgraded versions of the modules are in the new project ansible-modules(https://gitlab.com/logius/cloud-native-overheid/components/ansible-modules)

The modules in the new project are upgraded for kubernetes.core v2. Ansible v5 contains kubernetes.core v2.


